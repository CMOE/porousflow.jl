module NSF_test
using DrWatson
using Gridap
@quickactivate "PorousFlow.jl"

# Here you may include files from the source directory
include(srcdir("PorousFlow.jl"))
using .PorousFlow

run_NSF(NSF_params(verbose=true))

# Swirling flow Test
swirling_params = NSF_params(
  verbose=true,
  x₀=Point(0.0,-0.1e-3),
  x₁=Point(1.0e-3,0.4e-3),
  nx=100,
  ny=50,
  order=2,
  Re=600.0,
  Da=2.0e-5,
  β=1.0,
  T=1.0,
  Δt=0.01,
  vtkdir=joinpath(datadir(),"sims","NSF_swirling"))
run_NSF(swirling_params)

end
