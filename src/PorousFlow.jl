module PorousFlow
using Gridap
using Parameters
using Printf
using DrWatson

include("./Navier-Stokes-Forchheimer.jl")
export NSF_params, run_NSF

end # module PorousFlow
