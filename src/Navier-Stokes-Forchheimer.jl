"""
NSF_params

Parameters for the Navier-Stokes-Forchheimer solver.
"""
@with_kw struct NSF_params
  x₀::Point = Point(0.0,-0.1) # Bottom-left corner of the domain
  x₁::Point = Point(1.0,0.4)   # Top-right corner of the domain
  nx::Int64 = 4 # Number of grid elements in the x-direction
  ny::Int64 = 4 # Number of grid elements in the y-direction
  order::Int64 = 2 # Order of the velocity Finite Elements
  verbose::Bool = false # Whether to print extra information
  vtkdir::String = joinpath(datadir(),"sims","NSF_test") # Directory to store the VTK files
  Re::Float64 = 100.0 # Reynolds number
  Da::Float64 = 1.0 # Darcy number
  β::Float64 = 1.0 # viscosity ratio
  Δt::Float64 = 0.01 # Time step
  T::Float64 = 0.01 # Final time
  ρ∞::Float64 = 0.9 # Spectral radius of the generalized-alpha method
end

"""
run_NSF(params::NSF_params)

Runs the Navier-Stokes-Forchheimer solver with the given parameters.
"""
function run_NSF(params::NSF_params)

  # IO params
  @unpack verbose, vtkdir = params

  # Create the model
  @unpack x₀, x₁, nx, ny = params
  Lₓ = x₁[1]-x₀[1]
  domain = (x₀[1],x₁[1],x₀[2],x₁[2])
  partition = (nx,ny)
  model_Ω = CartesianDiscreteModel(domain,partition,isperiodic=(true,false))
  verbose && writevtk(model_Ω,vtkdir*"/model_Ω")

  # Triangulations
  Ω = Interior(model_Ω)

  # Labeling the boundaries
  labels_Ω = get_face_labeling(model_Ω)
  add_tag_from_tags!(labels_Ω,"top",[6])
  add_tag_from_tags!(labels_Ω,"bottom",[5])
  # add_tag_from_tags!(labels_Ω,"inlet",[7])
  # add_tag_from_tags!(labels_Ω,"outlet",[8])

  # Boundary conditions
  u_top((x,y), t::Real) = VectorValue(cos(2π*x/Lₓ),sin(2π*x/Lₓ))
  u_top(t::Real) = x -> u_top(x, t::Real)
  u_bottom((x,y), t::Real) = VectorValue(0.0,0.0)
  u_bottom(t::Real) = x -> u_bottom(x, t::Real)

  # Heaviside function
  H(ϕ) = ϕ > 0 ? 1.0 : 0.0

  # FE spaces
  @unpack order = params
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{2,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)
  reffeᵩ = ReferenceFE(lagrangian,Float64,order)
  V = TestFESpace(model_Ω,reffeᵤ,conformity=:H1,dirichlet_tags=["top", "bottom"])
  U = TransientTrialFESpace(V, [u_top, u_bottom])
  Q = TestFESpace(model_Ω,reffeₚ,conformity=:L2)
  P = TrialFESpace(Q)
  Ψ = TestFESpace(model_Ω,reffeᵩ,conformity=:H1)
  Φ = TransientTrialFESpace(Ψ)
  X = TransientMultiFieldFESpace([U,P,Φ])
  Y = MultiFieldFESpace([V,Q,Ψ])
  X₀ = MultiFieldFESpace([U(0.0),P])
  Y₀ = MultiFieldFESpace([V,Q])

  # Integration measures
  degree = 2*order
  dΩ = Measure(Ω,degree)

  # Initial level set function
  ϕ₀((x,y)) = -y
  ϕₕ₀ = interpolate_everywhere(ϕ₀,Φ)

  # Erosion model
  @unpack Re, Da, β = params
  Uₛ = 1.0
  α = 1.0
  d = 0.1
  ϵ = (5.6*(Da*Lₓ^2)/(d^2))^(1/5.5)
  n(∇ϕ) = ∇ϕ/norm(∇ϕ)
  nz = VectorValue(0.0,1.0)
  uₚ(u,∇ϕ) = max((α*u - Uₛ*nz)⋅n(∇ϕ),0.0)
  uᵢ(u,∇ϕ) = (uₚ(u,∇ϕ)*(1.0-ϵ)/Uₛ)*n(∇ϕ)
  ## Missing extension operator

  # Weak form
  # uᵢ = VectorValue(0.0,1.0e-4)
  res(t,(u,p,ϕ),(v,q,ψ)) = ∫( (H∘ϕ)*(1/(Re*Da)*(u⋅v) + 2/(β*Re)*(ε(u)⊙ε(v)) ) +
                              (1.0-H∘ϕ)*((∂t(u) + u⋅∇(u))⋅v + 2/Re*(ε(u)⊙ε(v)) ) +
                              q*(∇⋅u) - p*(∇⋅v) +
                              (∂t(ϕ) + (uᵢ∘(u,∇(ϕ)))⋅∇(ϕ))*ψ )dΩ
  op = TransientFEOperator(res,X,Y)
  a₀((u,p),(v,q)) = ∫( (H∘ϕₕ₀)*(1/(Re*Da)*(u⋅v) + 2/(β*Re)*(ε(u)⊙ε(v)) ) +
                          (1.0-H∘ϕₕ₀)*( 2/Re*(ε(u)⊙ε(v)) ) +
                          q*(∇⋅u) - p*(∇⋅v) )dΩ
  l₀((v,q)) = ∫( 0.0*q )dΩ
  op₀ = AffineFEOperator(a₀,l₀,X₀,Y₀)

  # Initial solution
  xₕ₀ = interpolate_everywhere([solve(op₀)...,ϕₕ₀],X(0.0))
  ∂ₜxₕ₀ = interpolate_everywhere([VectorValue(0.0,0.0),0.0,0.0],X(0.0))
  verbose && writevtk(Ω,vtkdir*"/sol0",cellfields=["u₀"=>xₕ₀[1],"p₀"=>xₕ₀[2],"ϕ₀"=>ϕₕ₀])

  # Solution
  @unpack Δt, T, ρ∞ = params
  nls = NLSolver(show_trace=verbose,method=:newton,iterations=10)
  ode_solver = GeneralizedAlpha(nls,Δt,ρ∞)
  xₕₜ = solve(ode_solver,op,(xₕ₀,∂ₜxₕ₀),0.0,T)

  # Velocity extension operator
  aᵤ((u,p),(v,q)) = ∫( ∇(u)⊙∇(v) - p*(∇⋅v) + q*(∇⋅u) )dΩ # + Add generalizaed WSBM

  # Potsprocess
  createpvd(vtkdir*"/sol") do pvd
    for ((uₕ,pₕ,ϕₕ),t) in xₕₜ
      verbose && (pvd[t] = createvtk(Ω,vtkdir*"/sol_"*@sprintf("%.5f",t)*".vtu",
        cellfields=["u"=>uₕ,"p"=>pₕ,"ϕ"=>ϕₕ,"uᵢ"=>uᵢ∘(uₕ,∇(ϕₕ))],order=order))
    end
  end

  return nothing
end
